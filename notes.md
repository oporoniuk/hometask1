# Hometask 1

## To add new home task to a new repository in Gitlab please follow the next steps:

1. Create a new remote repository (Hometask1) in Gitlab by pressing the button “Create new”
2. Create a new folder locally (Hometask1)
3. Open this new local folder in Terminal (you can also use the one in VS Code)
4. Create needed files in the local folder by using the button “New File”
5. In the terminal initialise the local git repository by typing a command `git init`
6. To set up our remote repository, type `git remote add origin git@gitlab.com:oporoniuk/hometask1.git`
7. To see the state of the working directory and the staging area type `git status`. Now we see all uncommitted changes.
8. Type `git add .` to add all changes that have been made in the folder to be ready for commit or type `git filename` to add some specific file
9. Type `git commit -m "adding hometask"` 
10. Type `git push origin main` to push all changes to gitlab
11. Go to your gitlab page, refresh it and you can see your newly added files in it

## To add an existing home task to a new remote repository please follow the next steps:

1. Create a new remote repository (Hometask1) in Gitlab by pressing the button “Create new”
2. Open your local folder with your home task in Terminal (you can also use the one in VS Code)
3. Follow all next steps from the previous descriptions starting from item 6